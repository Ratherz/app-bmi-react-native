// import * as React from "react";
// import { View, Text } from "react-native";

// export default function App() {
//   return (
//     <View
//       style={{
//         flex: 1,
//         justifyContent: "center",
//         alignItems: "center",
//       }}
//     >
//       <Text>Universal React with Expo</Text>
//     </View>
//   );
// }

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import firebase from 'firebase'
import apiKeys from './config/keys';
import WelcomeScreen from './src/views/Welcome';
import SignUp from './src/views/SignUp';
import SignIn from './src/views/SignIn';
import LoadingScreen from './src/views/Loading';
import Dashboard from './src/views/Dashboard';
import MyStack from './src/navigation/SwitchNavigator';

const Stack = createStackNavigator();

export default function App() {
  if (!firebase.apps.length) {
    console.log('Connected with Firebase')
    firebase.initializeApp(apiKeys.firebaseConfig);
  }

  return (
    <NavigationContainer>
      {/* <Stack.Navigator>
        <Stack.Screen name={'Loading'} component={LoadingScreen} options={{ headerShown: false }} />
        <Stack.Screen name='Home' component={WelcomeScreen} options={{ headerShown: false }} />
        <Stack.Screen name='Sign Up' component={SignUp} options={{ headerShown: false }} />
        <Stack.Screen name='Sign In' component={SignIn} options={{ headerShown: false }} />
        <Stack.Screen name={'Dashboard'} component={Dashboard} options={{ headerShown: false }} />
      </Stack.Navigator> */}
      <MyStack/>
    </NavigationContainer>
  );
}