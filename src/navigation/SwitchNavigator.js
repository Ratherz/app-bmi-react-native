import 'react-native-gesture-handler';
import React from 'react'
import { View, TouchableOpacity, Image } from 'react-native';
// import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
// import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from '@react-navigation/drawer';
import CustomSidebarMenu from './CustomSidebarMenu';
import SignIn from '../views/SignIn';
import SignUp from '../views/SignUp';
import Dashboard from '../views/Dashboard';
import Home from '../views/Home';
import LoadingScreen from '../views/Loading';

// const SwitchNavigator = NavigationContainer(
//     {
//         Login: {
//             screen: Login
//         },
//         Signup: {
//             screen: Signup
//         },
//         Profile: {
//             screen: Profile
//         }
//     },
//     {
//         initialRouteName: 'Login'
//     }
// )

// export default createStackNavigator(SwitchNavigator)

// const Stack = createStackNavigator();
// function MyStack() {
//     return (
//         <Stack.Navigator initialRouteName="Home">
//             <Stack.Screen name="Home" component={Home} />
//             <Stack.Screen name="Login" component={Login} />
//             <Stack.Screen name="Profile" component={Profile} />
//         </Stack.Navigator>
//     );
// }

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const NavigationDrawerStructure = (props) => {
    //Structure for the navigatin Drawer
    const toggleDrawer = () => {
        //Props to open/close the drawer
        props.navigationProps.toggleDrawer();
    };

    return (
        <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity onPress={toggleDrawer}>
                {/*Donute Button Image */}
                <Image
                    source={{
                        uri:
                            'https://raw.githubusercontent.com/AboutReact/sampleresource/master/drawerWhite.png',
                    }}
                    style={{ width: 25, height: 25, marginLeft: 5 }}
                />
            </TouchableOpacity>
        </View>
    );
};

function SignUpScreenStack({ navigation }) {
    return (
        <Stack.Navigator initialRouteName="SignUp">
            <Stack.Screen
                name="Home"
                component={Home}
                options={{
                    headerShown: false 
                    // title: 'Second Page', //Set Header Title
                }}
            />
            <Stack.Screen
                name="SignUp"
                component={SignUp}
                options={{
                    headerShown: false 
                    // title: 'Home', //Set Header Title
                    // headerLeft: () => (
                    //     <NavigationDrawerStructure navigationProps={navigation} />
                    // ),
                    // headerStyle: {
                    //     backgroundColor: '#f4511e', //Set Header color
                    // },
                    // headerTintColor: '#fff', //Set Header text color
                    // headerTitleStyle: {
                    //     fontWeight: 'bold', //Set Header text style
                    // },
                }}
            />
            <Stack.Screen
                name="SignIn"
                component={SignIn}
                options={{
                    headerShown: false 
                    // title: 'Second Page', //Set Header Title
                }}
            />
            <Stack.Screen
                name="Loading"
                component={LoadingScreen}
                options={{
                    headerShown: false 
                    // title: 'Second Page', //Set Header Title
                }}
            />
        </Stack.Navigator>
    );
}

function DashboardScreenStack({ navigation }) {
    return (
        <Stack.Navigator
            initialRouteName="Dashboard"
            screenOptions={{
                headerLeft: () => (
                    <NavigationDrawerStructure navigationProps={navigation} />
                ),
                headerStyle: {
                    backgroundColor: '#f4511e', //Set Header color
                },
                headerTintColor: '#fff', //Set Header text color
                headerTitleStyle: {
                    fontWeight: 'bold', //Set Header text style
                },
            }}>
            <Stack.Screen
                name="Dashboard"
                component={Dashboard}
                options={{
                    // title: 'Second Page', //Set Header Title
                    headerShown: false 
                }}
            />
            {/* <Stack.Screen
                name="Profile"
                component={Profile}
                options={{
                    // title: 'Third Page', //Set Header Title
                    headerShown: false 
                }}
            /> */}
        </Stack.Navigator>
    );
}

function MyStack() {
    return (
        // <Stack.Navigator initialRouteName="Home">
        //     <Stack.Screen name="Home" component={Home} />
        //     <Stack.Screen name="Login" component={Login} />
        //     <Stack.Screen name="Profile" component={Profile} />
        // </Stack.Navigator>
        <Drawer.Navigator
            drawerContentOptions={{
                activeTintColor: '#e91e63',
                itemStyle: { marginVertical: 5 },
            }}
            drawerContent={(props) => <CustomSidebarMenu {...props} />}>
            <Drawer.Screen
                name="SignUp"
                options={{
                    gestureEnabled: false,

                 }}
                component={SignUpScreenStack}
                />
            <Drawer.Screen
                name="Dashboard"
                // options={{ drawerLabel: 'Second page Option' }}
                component={DashboardScreenStack}
            />
        </Drawer.Navigator>
    );
}


export default MyStack;