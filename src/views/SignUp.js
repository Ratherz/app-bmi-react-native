import React, { useState } from 'react';
import { View, Text, TextInput, Alert, ScrollView, Keyboard, StyleSheet, SafeAreaView, TouchableOpacity, ImageBackground } from 'react-native';
import { Header, Input } from 'react-native-elements';
import { useFonts } from "expo-font"
import AppLoading from 'expo-app-loading';
import { RadioButton } from 'react-native-paper';

// import { TouchableOpacity } from 'react-native-gesture-handler';
import { registration } from '../../config/firebaseMethods';

export default function SignUp({ navigation }) {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [Phone, setPhone] = useState('');
    const [Gender, setGender] = React.useState('Male');

    const emptyState = () => {
        setFirstName('');
        setLastName('');
        setEmail('');
        setPassword('');
        setConfirmPassword('');
        setPhone('');
        setGender('');
    };

    let [fontsLoaded] = useFonts({
        'Lobster-Regular': require('../../assets/fonts/Lobster-Regular.ttf'),
    });


    const handlePress = () => {
        if (!firstName) {
            Alert.alert('First name is required');
        } else if (!email) {
            Alert.alert('Email field is required.');
        } else if (!password) {
            Alert.alert('Password field is required.');
        } else if (!confirmPassword) {
            setPassword('');
            Alert.alert('Confirm password field is required.');
        } else if (password !== confirmPassword) {
            Alert.alert('Password does not match!');
        } else {
            registration(
                email,
                password,
                lastName,
                firstName,
                Phone,
                Gender
            );
            // console.log("{Email: "+email+", password: "+password+", firstName: "+firstName+", lastName: "+lastName+", Gender: "+Checked+"}")
            navigation.navigate('Loading');
            emptyState();
        }
    };
    if (!fontsLoaded) {
        return <AppLoading />;
    } else {
        return (
            <SafeAreaView>
                <View style={styles.container}>
                    <ImageBackground source={require('../../assets/img/unsplash01.jpg')} style={styles.image} />
                    <View style={styles.titles}>
                        <Text style={styles.TextGroupL1}>Resgister</Text>
                    </View>
                    <ScrollView onBlur={Keyboard.dismiss}>
                        <View style={styles.inputGroupL1}>
                            <Input placeholder='First name' placeholderTextColor='#ffffff' style={styles.inputtext} value={firstName}
                                onChangeText={(name) => setFirstName(name)} />
                        </View>
                        <View style={styles.inputGroupL1}>
                            <Input placeholder='Last name' placeholderTextColor='#ffffff' style={styles.inputtext} value={lastName}
                                onChangeText={(name) => setLastName(name)} />
                        </View>
                        <View style={styles.inputGroupL1}>
                            <Input placeholder='Email' placeholderTextColor='#ffffff' style={styles.inputtext} value={email}
                                onChangeText={(email) => setEmail(email)} autoCapitalize="none" keyboardType="email-address" />
                        </View>
                        <View style={styles.inputGroupL2}>
                            <Input placeholder='Password' placeholderTextColor='#ffffff' style={styles.inputtext} secureTextEntry={true} value={password}
                                onChangeText={(password) => setPassword(password)} secureTextEntry={true} />
                        </View>
                        <View style={styles.inputGroupL2}>
                            <Input placeholder='Confrim Password' placeholderTextColor='#ffffff' style={styles.inputtext} value={confirmPassword} secureTextEntry={true}
                                onChangeText={(password2) => setConfirmPassword(password2)} />
                        </View>
                        <View style={styles.inputGroupL2}>
                            <Input placeholder='Telephone' placeholderTextColor='#ffffff' style={styles.inputtext} value={Phone}
                                onChangeText={(phone) => setPhone(phone)} />
                        </View>
                        <RadioButton.Group onValueChange={newValue => setGender(newValue)} value={Gender}>
                            <View style={{ paddingHorizontal: 50, marginVertical: 16 }}>
                                <Text style={{ color: '#FFFFFF', fontSize: 20, }} >Gender</Text>
                                <View style={{ display: 'flex', flexDirection: "row", alignItems: "center" }}>
                                    <RadioButton value="Male" />
                                    <Text style={{ color: '#FFFFFF', fontSize: 20, }}>Male</Text>
                                </View>
                                <View style={{ display: 'flex', flexDirection: "row", alignItems: "center" }}>
                                    <RadioButton value="Female" color='purple'  />
                                    <Text style={{ color: '#FFFFFF', fontSize: 20, }}>Female</Text>
                                </View>
                            </View>
                        </RadioButton.Group>
                        <TouchableOpacity style={styles.btnSingUp} onPress={handlePress}>
                            <Text style={{ color: '#FFFFFF', fontSize: 18, }} >Sign Up</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnLogin} onPress={() =>  navigation.navigate('SignIn')}>
                            <Text style={{ color: '#FFFFFF', fontSize: 18, }} >Sign In</Text>
                        </TouchableOpacity>
                        {/* </View> */}
                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }
}


// const styles = StyleSheet.create({
//     container: {
//         height: '100%',
//         width: '100%',
//         backgroundColor: '#3FC5AB',
//         alignItems: 'center',
//         justifyContent: 'center',
//     },
//     button: {
//         width: 200,
//         padding: 5,
//         backgroundColor: '#ff9999',
//         borderWidth: 2,
//         borderColor: 'white',
//         borderRadius: 15,
//         alignSelf: 'center',
//         margin: '5%',
//     },
//     buttonText: {
//         fontSize: 20,
//         color: 'white',
//         fontWeight: 'bold',
//         textAlign: 'center',
//     },
//     inlineText: {
//         fontSize: 20,
//         fontWeight: 'bold',
//         color: 'navy',
//         textAlign: 'center',
//         marginTop: '5%',
//     },
//     text: {
//         textAlign: 'center',
//         fontSize: 25,
//         margin: '5%',
//         marginTop: '15%',
//         fontWeight: 'bold',
//         color: '#2E6194',
//     },
//     textInput: {
//         width: 300,
//         fontSize: 18,
//         borderWidth: 1,
//         borderColor: '#a4eddf',
//         padding: 10,
//         margin: 5,
//     },
// });


const styles = StyleSheet.create({
    // container: {
    //   flex: 1,
    //   justifyContent: "center",
    //   padding: 50,
    //   backgroundColor: "#eaeaea"
    // }

    container: {
        width: '100%',
        height: '100%',
        // flex: 1,
        // flexDirection: "column"
    },
    inputGroupL1: {
        marginTop: 0,
        paddingHorizontal: 50,
        color: '#FFFFFF'


    },
    inputtext: {
        color: '#FFFFFF',
    },
    inputGroupL2: {
        marginTop: 10,
        paddingLeft: 50,
        paddingRight: 50,

    },
    btnLogin: {
        marginBottom: 20,
        alignItems: 'center',
        backgroundColor: '#00BFFF',
        padding: 10,
        borderRadius: 50,
        color: '#fff',
        marginLeft: 50,
        marginRight: 50

    },
    btnSingUp: {
        marginBottom: 10,
        alignItems: 'center',
        backgroundColor: '#FFA500',
        padding: 10,
        borderRadius: 50,
        color: '#fff',
        marginLeft: 50,
        marginRight: 50
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
        position: 'absolute',
        width: '100%',
        height: '100%'
    },
    TextGroupL1: {
        fontFamily: 'Lobster-Regular',
        fontSize: 40,
        // paddingTop: 20
        // marginTop: '2%'
        textAlignVertical: 'top',
        color: '#FFFFFF'
    },
    TextGroupL2: {
        fontFamily: 'Lobster-Regular',
        fontSize: 40,
        textAlignVertical: 'top'
        // marginBottom: ''
    },
    TextGroupL3: {
        // fontFamily: 'Lobster-Regular',
        fontSize: 20,
        color: '#FFFFFF',
        marginTop: '5'
        // textAlignVertical: 'top'
        // marginBottom: ''
    },
    titles: {
        alignItems: 'center',
        marginTop: '15%',
    },
    RadioButton: {
        alignItems: 'center',
    }

});