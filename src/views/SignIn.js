import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, Alert, ImageBackground, TouchableOpacity } from 'react-native';
import { Header, Input } from 'react-native-elements';
// import { TouchableOpacity } from 'react-native-gesture-handler';
import { signIn } from '../../config/firebaseMethods';
import { useFonts } from "expo-font"
import AppLoading from 'expo-app-loading';

export default function SignIn({ navigation }) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    let [fontsLoaded] = useFonts({
        'Lobster-Regular': require('../../assets/fonts/Lobster-Regular.ttf'),
    });

    const handlePress = () => {
        if (!email) {
            Alert.alert('Email field is required.');
        }

        if (!password) {
            Alert.alert('Password field is required.');
        }

        signIn(email, password).then(() => navigation.navigate('Loading', { screen: 'Loading' }));
        setEmail('');
        setPassword('');
    };

    if (!fontsLoaded) {
        return <AppLoading />;
    } else {
        return (
            <View style={styles.container}>
                <ImageBackground source={require('../../assets/img/unsplash.jpg')} style={styles.image} />
                <View style={styles.titles}>
                    <Text style={styles.TextGroupL1}>Calculate</Text>
                    <Text style={styles.TextGroupL2}>Body Mass Index</Text>
                </View>
                <View style={styles.inputGroupL1}>
                    <Input placeholder='Email' style={styles.inputtext} value={email}
                        onChangeText={(email) => setEmail(email)} autoCapitalize="none" keyboardType="email-address" />
                </View>
                <View style={styles.inputGroupL2}>
                    <Input placeholder='Password' style={styles.inputtext} secureTextEntry={true} value={password}
                        onChangeText={(password) => setPassword(password)} secureTextEntry={true} />
                </View>
                <TouchableOpacity style={styles.btnLogin} onPress={handlePress}>
                    <Text style={{ color: '#FFFFFF', fontSize: 18, }}>Login</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.btnSingUp} onPress={() => navigation.navigate('SignUp')}>
                    <Text style={{ color: '#FFFFFF', fontSize: 18, }}>Sing Up</Text>
                </TouchableOpacity>
            </View>
        );
    }

}

const styles = StyleSheet.create({

    container: {
        width: '100%',
        height: '100%',
        // flex: 1,
        // flexDirection: "column"
    },
    inputGroupL1: {
        marginTop: 60,
        paddingLeft: 50,
        paddingRight: 50,


    },
    inputGroupL2: {
        marginTop: 10,
        paddingLeft: 50,
        paddingRight: 50,

    },
    btnLogin: {
        marginBottom: 20,
        alignItems: 'center',
        backgroundColor: '#00BFFF',
        padding: 10,
        borderRadius: 50,
        color: '#fff',
        marginLeft: 50,
        marginRight: 50

    },
    btnSingUp: {
        marginBottom: 10,
        alignItems: 'center',
        backgroundColor: '#FFA500',
        padding: 10,
        borderRadius: 50,
        color: '#fff',
        marginLeft: 50,
        marginRight: 50
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
        position: 'absolute',
        width: '100%',
        height: '100%'
    },
    TextGroupL1: {
        fontFamily: 'Lobster-Regular',
        fontSize: 40,
        // paddingTop: 20
        // marginTop: '2%'
        textAlignVertical: 'top'
    },
    TextGroupL2: {
        fontFamily: 'Lobster-Regular',
        fontSize: 40,
        textAlignVertical: 'top'
        // marginBottom: ''
    },
    titles: {
        alignItems: 'center',
        marginTop: '15%',
    }

});
