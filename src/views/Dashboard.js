import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Alert, ImageBackground } from 'react-native';
import { Header, Input, Card } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
import firebase from 'firebase';
import { AddBmi } from '../../config/firebaseMethods';

export default function Dashboard({ navigation }) {
    let currentUserUID = firebase.auth().currentUser.uid;
    const [firstName, setFirstName] = useState('');
    const [userWeight, setuserWeight] = useState('');
    const [userHeight, setuserHeight] = useState('');
    const [Result, setResult] = useState('');
    const [bmiTitle, setbmiTitle] = useState('');
    const [bmiText, setbmiText] = useState('');

    useEffect(() => {
        async function getUserInfo() {
            let doc = await firebase
                .firestore()
                .collection('users')
                .doc(currentUserUID)
                .get();

            if (!doc.exists) {
                Alert.alert('No user data found!')
            } else {
                let dataObj = doc.data();
                setFirstName(dataObj.firstName)
            }
        }
        getUserInfo();
    })


    const CalBmi = () => {
        let m = userHeight * 0.01;
        let result = userWeight / (Math.pow(m, 2));

        setResult(result.toFixed(2));
        if (result.toFixed(2) <= 18.5) {
            setbmiTitle('เกณฑ์น้ำหนักน้อยกว่ามาตรฐาน');
            setbmiText(' คุณควรรับประทานอาหารประเภท ไขมัน เเป้ง เนื้อสัตว์ มากขึ้นเนื่องจากน้ำหนักน้อยเกินไป ขณะเดียวกันคุณก็ต้องเพิ่มกล้ามเนื้อด้วย โดยออกกำลังกายเบาๆ ไม่หนักมากเช่น เดินเร็ว ปั่นจักรยาน');
            return AddBmi(userWeight, userHeight, result.toFixed(2), 'เกณฑ์น้ำหนักน้อยกว่ามาตรฐาน');
        } else if (result.toFixed(2) >= 18.50 && result.toFixed(2) <= 22.90) {
            setbmiTitle('เกณฑ์น้ำหนักปกติ');
            setbmiText('ร่างกายคุณปกติอยู่เเล้ว เเสดงถึงลักษณะการรับประทานอาหารที่เพียงพอต่อร่างกาย ไม่ขาดไม่เกินจนเกินไป เเละควรทานเมล็ดธัญพืชเช่น ถั่ว เเอลมอนต์ ให้ร่างกายได้รับไขมันดี');
            return AddBmi(userWeight, userHeight, result.toFixed(2), 'เกณฑ์น้ำหนักปกติ');
        } else if (result.toFixed(2) >= 23 && result.toFixed(2) <= 24.90) {
            setbmiTitle('เกณฑ์รูปร่างท้วม / อ้วนระดับ 1');
            setbmiText('เมื่ออยู่ในเกณฑ์ท้วม เเสดงว่าคุณได้รับสารอาหารมากเกินไป จนไขมันสะสมเกาะตามส่วนตามส่วนต่างๆ โดยคุณควรลดอาหารประเภทเเป้ง เนื้อสัตว์ ขนมหวาน เเละทานผลไม้มากขึ้น ส่วนกีฬาอย่างต่ำ 3 วัน/สัปดาห์ เป็นกีฬาที่เผาผลาญพลังงานเยอะ รวมถึงกีฬาประเภท Body Weight');
            return AddBmi(userWeight, userHeight, result.toFixed(2), 'เกณฑ์รูปร่างท้วม / อ้วนระดับ 1');
        } else if (result.toFixed(2) >= 25 && result.toFixed(2) <= 29.90) {
            setbmiTitle('เกณฑ์รูปร่างอ้วน / อ้วนระดับ 2');
            setbmiText(' ถึงเเม้คุณจะอยู่ในเกณฑ์อ้วน เเต่หากคุณตระหนักถึงความสำคัญเรื่องสุขภาพก็ไม่ต้องกังวลไป เพราะคุณสามารถกลับมามีสุขภาพดี เพียงควบคุมเเคลอรี่อาหารต่อหนึ่งวัน ให้อยู่ในระหว่าง 1,600 – 2,000 เเคลอรี่ ออกกำลังกายมากกว่า 3 วัน/สัปดาห์ เเละมีวินัยพอที่จะทำอย่างสม่ำเสมอ');
            return AddBmi(userWeight, userHeight, result.toFixed(2), 'เกณฑ์รูปร่างอ้วน / อ้วนระดับ 2');
        } else if (result.toFixed(2) >= 30) {
            setbmiTitle('เกณฑ์รูปร่างอ้วนมาก / อ้วนระดับ 3');
            setbmiText('เมื่อเกณฑ์รูปร่างคุณอยู่ขั้นนี้เเสดงถึงลักษณะการกินที่ไม่ถูกกิจลักษณะหรือบริโภคต่อวันมากเกินไป สิ่งที่ควรละเว้นเลยคือของทอด ขนมปัง บ่อเกิดเเห่งไขมันชั้นดี เลือกทานอาหารปรุงน้อยหรือไม่ใส่น้ำมันเลย ส่วนเนื้อสัตว์ควรทาน เนื้อปลา อกไก่ ที่สำคัญคือการออกกำลังกาย เมื่อถึงขั้นนี้เเล้วการออกกำลังกายอาจไม่ใช่กิจกรรมที่คุณทำบ่อยมากนัก เเต่เน้นกีฬาที่ชอบเเละสม่ำเสมอ ');
            return AddBmi(userWeight, userHeight, result.toFixed(2), 'เกณฑ์รูปร่างอ้วนมาก / อ้วนระดับ 3');
        }


    }

    return (
        <View style={{ flex: 1 }}>
            <ImageBackground source={require('../../assets/img/unsplash02.jpg')} imageStyle=
                {{ opacity: 0.9 }} style={{
                    flex: 1,

                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    backgroundColor: 'rgb(9,9,9)'
                }} />
            <View>
                <Header
                    leftComponent={
                        <Icon name='bars' size={24} color='#ffffff' onPress={() => navigation.openDrawer()} />
                    }
                    centerComponent={{ text: 'Calculate BMI', style: { fontFamily: 'Lobster-Regular', color: '#fff' } }}
                    containerStyle={{
                        backgroundColor: '#3D6DCC',
                        justifyContent: 'space-around',
                    }}
                />
            </View>
            <View
                style={{
                    padding: 30,
                    backgroundColor: 'rgba(255, 255, 255,0.75)',
                    margin: 50,
                    borderRadius: 30,
                    flexDirection: 'column',
                    top: 50,
                }}>
                <Input placeholder='น้ำหนัก' value={userWeight} onChangeText={(userWeight) => setuserWeight(userWeight)} />
                <Input placeholder='ส่วนสูง' value={userHeight} onChangeText={(userHeight) => setuserHeight(userHeight)} />
                <Text style={{ fontSize: 20, textAlign: 'center', fontWeight: 'bold', }}>{Result}</Text>
                <TouchableOpacity style={styles.button} onPress={CalBmi} >
                    <Text style={styles.buttonText}>คำนวณ</Text>
                </TouchableOpacity>

            </View>
            <Card style={styles.CardBmi}>
                <Card.Title style={{ fontSize: 20, textAlign: 'center', fontWeight: 'bold', }}>คำแนะนำ</Card.Title>
                <Card.Divider />
                <Text style={{ fontSize: 17, textAlign: 'left', fontWeight: 'bold', }}>{bmiTitle}</Text>
                <Text style={{ marginBottom: 10 }}>{bmiText}</Text>
            </Card>
        </View>
    )
}

const styles = StyleSheet.create({
    button: {
        width: 150,
        padding: 5,
        backgroundColor: '#ff9999',
        borderWidth: 2,
        borderColor: 'white',
        borderRadius: 15,
        alignSelf: 'center',
    },
    buttonText: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    container: {
        height: '100%',
        width: '100%',
        backgroundColor: '#3FC5AB',
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        textAlign: 'center',
        fontSize: 20,
        fontStyle: 'italic',
        marginTop: '2%',
        marginBottom: '10%',
        fontWeight: 'bold',
        color: 'black',
    },
    titleText: {
        textAlign: 'center',
        fontSize: 30,
        fontWeight: 'bold',
        color: '#2E6194',
    },
    CardBmi: {
        display: 'none'
    },
});